package com.qweex.but_of_chords;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends ListActivity implements AdapterView.OnItemClickListener {

    public static List<Song> chords = new ArrayList<Song>();
    public static Map<Song, String> filenames = new HashMap<Song, String>();

    public static File SD_DIR;

    Comparator<Song> SortTitle = new Comparator<Song>() {
        @Override
        public int compare(Song A, Song B) {
            if(A.title()!=null && B!=null && B.title()!=null)
                return A.title().compareTo(B.title());
            return SortSubTitle.compare(A, B);
        }
    };
    Comparator<Song> SortSubTitle = new Comparator<Song>() {
        @Override
        public int compare(Song A, Song B) {
            if(A.subtitle()!=null && B!=null && B.subtitle()!=null) {
                if(A.subtitle().equals(B.subtitle()))
                    return SortTitle.compare(A, B);
                return A.subtitle().compareTo(B.subtitle());
            }
            int r = 0;
            if(B==null || B.subtitle()==null)
                r -= 1;
            if(A.subtitle()==null)
                r += 1;
            return r;
        }
    };

    Comparator<Song> sortOrder = SortSubTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chords.clear();

        SD_DIR = new File(Environment.getExternalStorageDirectory(),
                getResources().getString(R.string.app_name));
        SD_DIR.mkdir();
        getListView().setOnItemClickListener(this);
        getListView().setFastScrollEnabled(true);
        getListView().setFastScrollAlwaysVisible(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("BoC", SD_DIR.getAbsolutePath());
        chords.clear();
        for(File file : SD_DIR.listFiles()) {
            Log.d("BoC: file", file.toString());
            try {
                Song song = new Song(new FileReader(file));
                chords.add(song);
                filenames.put(song, file.getName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        Collections.sort(chords, sortOrder);
        setListAdapter(new ChordAdapter(MainActivity.this, chords));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent viewer = new Intent(MainActivity.this, SongViewer.class);
        viewer.putExtra("position", i);
        startActivity(viewer);
    }

    class ChordAdapter extends ArrayAdapter<Song> implements SectionIndexer {
        List<Song> songs;
        List<String> sections = new ArrayList<String>();
        String[] sectionsArr;


        public ChordAdapter(Context c, List<Song> data) {
            super(c, android.R.layout.simple_list_item_2, android.R.id.text1, data);
            songs = data;
            String last = null;
            for(Song song : songs) {
                String next = sectionOf(song);
                if(next==null || last.compareTo(next) != 0) {
                    sections.add(next);
                    last = next;
                }
                Log.d("Section:", next);
            }
            sectionsArr = new String[ sections.size() ];
            sections.toArray(sectionsArr);
        }

        String sectionOf(Song song) {
            try {
                if (sortOrder == SortSubTitle)
                    return song.subtitle().substring(0, 1).toUpperCase();
                else
                    return song.title().substring(0, 1).toUpperCase();
            } catch(NullPointerException e) {
                return "";
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = super.getView(position, convertView, parent);

            String title = songs.get(position).title();
            if(title==null)
                title = songs.get(position).subtitle();
            if(title==null)
                title = filenames.get(songs.get(position));
            String subtitle = songs.get(position).subtitle();

            ((TextView) convertView.findViewById(android.R.id.text1)).setText(title);
            ((TextView) convertView.findViewById(android.R.id.text2)).setText(subtitle);

            ((TextView)convertView.findViewById(android.R.id.text1)).setEllipsize(TextUtils.TruncateAt.END);
            ((TextView)convertView.findViewById(android.R.id.text2)).setEllipsize(TextUtils.TruncateAt.END);
            convertView.findViewById(android.R.id.text2).setPadding(20, 0, 0, 0);
            return convertView;
        }

        @Override
        public String[] getSections() {
            return sectionsArr;
        }

        @Override
        public int getPositionForSection(int i) {
            Song song = this.getItem(i);
            return sections.indexOf( sectionOf(song) );
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }
    }


    final int NEW = 0, SORT = 1, TITLE = 2, SUBTITLE = 3;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, NEW, 0, "New");
        Menu sort = menu.addSubMenu(0, SORT, 0, "Sort");
        sort.add(0, TITLE, 0, "Title");
        sort.add(0, SUBTITLE, 0, "Subtitle");

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==TITLE || item.getItemId()==SUBTITLE) {
            sortOrder = item.getItemId()==TITLE ? SortTitle : SortSubTitle;
            Collections.sort(chords, sortOrder);
            ((ChordAdapter)getListAdapter()).notifyDataSetChanged();
            return super.onOptionsItemSelected(item);
        }
        if(item.getItemId()!=NEW)
            return super.onOptionsItemSelected(item);

        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);

        final EditText filename = new EditText(this);
        final EditText title = new EditText(this);
        final EditText subtitle = new EditText(this);

        filename.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        title.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        subtitle.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        filename.setHint("Filename (Required)");
        title.setHint("Title");
        subtitle.setHint("Subtitle");

        ll.addView(filename);
        ll.addView(title);
        ll.addView(subtitle);

        new AlertDialog.Builder(this)
                .setView(ll)
                .setTitle("New File")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent editor = new Intent(MainActivity.this, SongEditor.class);
                                String name = filename.getText().toString().trim();
                                if(!name.toLowerCase().matches(".+.(txt|chordpro|cho|pro|chopro|crd)$"))
                                    name = name + ".txt";
                                editor.putExtra("filename", name);
                                StringBuilder sb = new StringBuilder();
                                if (title.getText().toString().trim().length() >0) {
                                    sb.append("{title: ").append(title.getText().toString()).append("}\n");
                                    editor.putExtra("title", title.getText().toString().trim());
                                }
                                if (subtitle.getText().toString().trim().length() >0) {
                                    sb.append("{subtitle: ").append(subtitle.getText().toString().trim()).append("}\n");
                                    editor.putExtra("subtitle", subtitle.getText().toString().trim());
                                }
                                if(sb.length()>0)
                                    editor.putExtra("text", sb.toString());
                                startActivityForResult(editor, 0);
                            }
                        }

                )
                .show();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK && data.getStringExtra("text")!=null) {
            chords.add(new Song(new StringReader(data.getStringExtra("text"))));
            Collections.sort(chords, sortOrder);
        }
        ((ArrayAdapter)getListAdapter()).notifyDataSetChanged();
    }
}

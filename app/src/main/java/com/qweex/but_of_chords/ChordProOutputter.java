package com.qweex.but_of_chords;


public class ChordProOutputter extends Outputter {

    @Override
    public String chord(Song.Chord chord) {
        return "[" + chord.transpose(song.key) + "]";
    }

    @Override
    public String word(Song.Word word) {
        return (word.chord()!=null ? chord(word.chord()) : "") + word.lyric;
    }

    @Override
    public String line(Song.Line line) {
        //FIXME: Spaces were inserted between syllables; tried a fix, untested
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for(Song.Word word : line.words) {
            if(!first && !word.latterSyllable)
                sb.append(' ');
            sb.append(word(word));
            first = false;
        }
        return sb.toString();
    }

    @Override
    public String lineDirective(Song.LineDirective line) {
        StringBuilder sb = new StringBuilder();
        sb.append("{").append(line.getDirective());
        if(line.getDirective().hasArg())
            sb.append(": ").append(line.arg);
        return sb.append("}").toString();
    }

    @Override
    public String lineRaw(Song.LineRaw line) {
        return line.raw;
    }

    @Override
    public String lineComment(Song.LineComment line) {
        return "# " + line.comment;
    }

}

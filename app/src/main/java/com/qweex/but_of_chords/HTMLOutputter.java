package com.qweex.but_of_chords;


import android.util.Log;

public class HTMLOutputter extends Outputter {
    static final String CSS =
            "chord { color: blue; font-weight: bold; }\n"
                    + "word chord { position: absolute; margin-top: -1em; }\n"
//            + "word { margin-right: 0.25em; }\n"
                    + "solo { margin-right: 1em; }\n"
                    + "line { display: block; line-height: 3em;}\n"
                    + "line.no-chords { line-height: 1em }\n"
//                    + "line.no-chords word { margin-right: 1em }\n"
                    + "h1, h3 { padding: 0; margin: 0; }\n"
                    + "tab { font-family: monospace; }\n"
                    + "chorus { color: #333; }\n"
                    + "body { font-size: 0.8em; }\n"
            ;//"section:not(:last-child) { padding-bottom: 2em; }";

    @Override
    public String song(Song song) {
        StringBuilder sb = new StringBuilder()
                .append("<html><body><style>")
                .append(CSS)
                .append("</style>\n");
        if(song.title!=null)
            sb.append("<h1>").append(song.title()).append("</h1>");
        if(song.subtitle!=null)
            sb.append("<h3>").append(song.subtitle()).append("</h3>");
        sb.append( super.song(song) );
        return sb
                .append("</body></html>")
                .toString();
    }

    @Override
    public String chord(Song.Chord line) {
        return "<chord>" + line.transpose(song.key) + "</chord>";
    }

    @Override
    public String word(Song.Word word) {
        StringBuilder sb = new StringBuilder();
        if(word.chord!=null)
            sb.append(chord(word.chord));
        //TODO: A "solo" chord at the end of a normal line is not bumped up above.
        //  A possible solution would be to, for solos, output the chord(word.chord),
        //  then make it white so as to be invisible. Or just invisible via css.
        //  But that's hackish.
        if((word.lyric!=null && word.lyric.length()>0)) {
            sb.append(word.lyric).insert(0, "<word>").append("</word>");
            if(word.latterSyllable)
                sb.insert(0, "- ");
        } else {
            sb.insert(0, "<solo>").append("&nbsp;</solo>");
        }
        return sb.toString();
    }

    @Override
    public String line(Song.Line line) {
        boolean hasChords = false;
        StringBuilder sb = new StringBuilder();
        for(Song.Word word : line.words) {
            sb.append(word(word)).append(' ');
            hasChords |= word.hasChord();
        }
        Log.d("hasChords?", hasChords + " = " + sb.toString());
        if(!hasChords)
            sb.insert(0, "<line class='no-chords'>");
        else
            sb.insert(0, "<line>");

        if(line.words.size()<=1)
            return "</br>";
        return sb.append("</line>")
                .toString();
    }

    @Override
    public String lineDirective(Song.LineDirective line) {
        Song.Directive directive = line.getDirective();
        String tag = directive.getTag();
        StringBuilder sb = new StringBuilder();
        if(tag!=null) {
            if(tag.equals("tab")) {
                if(directive.isStart())
                    sb.append("<tab><pre><code>");
                else
                    sb.append("</code></pre></tab>");
            } else
                sb.append("<").append(directive.isEnd() ? "/" : "").append(tag).append(">");
        }
        return sb.toString();
    }

    @Override
    public String lineRaw(Song.LineRaw line) {
        return line.raw + "<br/>";
    }

    @Override
    public String lineComment(Song.LineComment line) {
        return "<!-- " + line.comment + " -->";
    }
}

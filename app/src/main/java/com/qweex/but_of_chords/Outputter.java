package com.qweex.but_of_chords;


public abstract class Outputter {

    abstract public String chord(Song.Chord chord);
    abstract public String word(Song.Word word);
    abstract public String line(Song.Line line);
    abstract public String lineDirective(Song.LineDirective line);
    abstract public String lineRaw(Song.LineRaw line);
    abstract public String lineComment(Song.LineComment line);

    Song song;

    public String song(Song song) {
        this.song = song;
        StringBuilder sb = new StringBuilder();
        String lineStr;
        for(Song.LineBase line : song.lines) {
            if((lineStr = lineBase(line)) != null)
                sb.append(lineStr).append('\n');
        }
        return sb.toString();
    }

    protected String lineBase(Song.LineBase lineBase) {
        if(lineBase instanceof Song.LineDirective)
            return lineDirective((Song.LineDirective) lineBase);
        if(lineBase instanceof Song.LineRaw)
            return lineRaw((Song.LineRaw) lineBase);
        if(lineBase instanceof Song.LineComment)
            return lineComment((Song.LineComment) lineBase);
        return line((Song.Line) lineBase);
    }


}

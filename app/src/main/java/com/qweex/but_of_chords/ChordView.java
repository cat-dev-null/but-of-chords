package com.qweex.but_of_chords;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;

class ChordView extends View {
    final int NUM_STRINGS, NUM_FRETS;
    private final int LINE_WIDTH_DP = 3;

    int[] stringXs, fretYs;

    Paint stringPaint, fretPaint, headstockPaint, chordPaint;
    int[] chord;

    public ChordView(Context context, int[] chord) {
        this(context, chord, 5);
    }

    public ChordView(Context context, int[] chord, int numFrets) {
        super(context);

        if(numFrets < 1)
            throw new IllegalArgumentException("Invalid number of frets: " + numFrets);

        NUM_FRETS = numFrets;
        NUM_STRINGS = chord.length;

        this.chord = new int[NUM_STRINGS];
        stringXs = new int[NUM_STRINGS];
        fretYs = new int[NUM_FRETS+1];

        System.arraycopy(chord, 0, this.chord, 0, NUM_STRINGS);

        stringPaint = new Paint();
        stringPaint.setStrokeWidth(px(LINE_WIDTH_DP));
        fretPaint = new Paint();
        fretPaint.setStrokeWidth(px(LINE_WIDTH_DP));
        headstockPaint = new Paint();
        headstockPaint.setStrokeWidth(px(LINE_WIDTH_DP) * 5);
        chordPaint = new Paint();

        setStringColor(Color.GRAY);
        setFretColor(Color.BLACK);
        setChordColor(Color.BLACK);
        setBackgroundColor(Color.WHITE);
    }

    public void setStringColor(int c) {
        stringPaint.setColor(c);
    }

    public void setFretColor(int c) {
        fretPaint.setColor(c);
        headstockPaint.setColor(c);
    }

    public void setChordColor(int c) {
        chordPaint.setColor(Color.BLACK);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int marginX, marginY;
        if(canvas.getHeight()>canvas.getWidth()) {
            marginX = canvas.getWidth() / 10;
            marginY = canvas.getHeight() / 20;
        } else {
            marginY = canvas.getWidth() / 10;
            marginX = canvas.getHeight() / 20;
        }


        // Draw top of fretboard
        canvas.drawLine(
                0,
                marginY,
                canvas.getWidth(),
                marginY,
                headstockPaint);
        fretYs[0] = marginY;


        //Draw the frets
        int availableSpacePx = (canvas.getHeight() - marginY*2) / NUM_FRETS;
        for(int fretNo = 1; fretNo <= NUM_FRETS; fretNo++) {
            int y = fretNo * availableSpacePx + marginY;
            fretYs[fretNo] = y;
            Log.d("y=", y + "<" + canvas.getHeight());
            canvas.drawLine(
                    0,
                    y,
                    canvas.getWidth(),
                    y,
                    fretPaint
            );
        }


        // Draw strings
        availableSpacePx = (int) ((canvas.getWidth() - marginX*2 - headstockPaint.getStrokeWidth()/2) / (NUM_STRINGS-1));
        for(int stringNo = 0; stringNo < NUM_STRINGS; stringNo++) {
            int x = stringNo * availableSpacePx + marginX;
            stringXs[stringNo] = x;
            canvas.drawLine(
                    x,
                    px(0),
                    x,
                    canvas.getHeight(),
                    stringPaint);
        }

        // Draw the chords
        // Example: Am
        int radius = availableSpacePx / 6;
        for(int strNum = 0; strNum<NUM_STRINGS; strNum++) {
            int fretNum = chord[strNum];
            if (fretNum > 0) {
                canvas.drawCircle(
                        stringXs[strNum],
                        (fretYs[fretNum] + fretYs[fretNum-1]) / 2,
                        radius,
                        chordPaint);
            }
        }
    }

    public int px(int dps) {
        return (int) (dps * getResources().getDisplayMetrics().density);
    }
}